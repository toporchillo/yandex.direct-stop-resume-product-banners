<?php
/**
 * @category   OpenCart
 * @package    Yandex.Direct
 * @copyright  Copyright (c) 2014 Alexander Toporkov (http://sourcedistillery.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License, Version 3
 */
?>
<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb["separator"]; ?><a href="<?php echo $breadcrumb["href"]; ?>"><?php echo $breadcrumb["text"]; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?> 
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
	
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
	<tr>
	  <td><?php echo $entry_id; ?></td>
	  <td><input type="text" name="yandex_direct_id" id="yandex_direct_id" value="<?php echo $yandex_direct_id; ?>" size="40" />
		<a href="https://oauth.yandex.ru/client/new" target="_blank"> <?php echo $entry_register_id; ?></a> /
		<a href="https://oauth.yandex.ru/client/my" target="_blank"> <?php echo $entry_find_id; ?></a>
	  <br/><span class="help"><?php echo $entry_id_help; ?></span>
	  </td>
	</tr>
	<tr>
	  <td><?php echo $entry_password; ?></td>
	  <td><input type="password" name="yandex_direct_password" value="<?php echo $yandex_direct_password; ?>" size="40" /></td>
	</tr>
<?php if ($yandex_direct_id && $yandex_direct_password) { ?>
	<tr>
	  <td><?php echo $entry_token; ?></td>
	  <td><input type="text" name="yandex_direct_token" value="<?php echo $yandex_direct_token; ?>" size="40" /> <a href="javascript:void(0);" id="get_token"><?php echo $entry_get_token; ?></a></td>
	</tr>
	<tr>
	  <td><?php echo $entry_campaign_id; ?></td>
	  <td><input type="text" name="yandex_direct_campaign_id" value="<?php echo $yandex_direct_campaign_id; ?>" size="40" /> <a href="https://direct.yandex.ru/registered/main.pl" target="_blank"><?php echo $entry_find_id; ?></a></td>
	</tr>

        <tr>
          <td><?php echo $entry_stop_status; ?></td>
          <td><select name="yandex_direct_stop_status[]" size="6" multiple>
              <?php foreach ($stock_statuses as $stat=>$stat_name) { ?>
              <option value="<?php echo $stat_name; ?>"<?php echo (is_array($yandex_direct_stop_status) && in_array($stat_name, $yandex_direct_stop_status) ? ' selected="selected"' : ''); ?>><?php echo $stat_name; ?></option>
	      <?php } ?>
            </select></td>
		</tr>
        <tr>
          <td><?php echo $entry_start_status; ?></td>
          <td><select name="yandex_direct_start_status[]" size="6" multiple>
              <?php foreach ($stock_statuses as $stat=>$stat_name) { ?>
              <option value="<?php echo $stat_name; ?>"<?php echo (is_array($yandex_direct_start_status) && in_array($stat_name, $yandex_direct_start_status) ? ' selected="selected"' : ''); ?>><?php echo $stat_name; ?></option>
	      <?php } ?>
            </select></td>
		</tr>
        <tr>
          <td><?php echo $entry_number; ?></td>
          <td><input type="text" name="yandex_direct_number" value="<?php echo intval($yandex_direct_number); ?>" />
          </td>
		</tr>
        <tr>
          <td><?php echo $entry_unarchive; ?></td>
          <td><input type="checkbox" name="yandex_direct_unarchive" value="1" <?php echo ($yandex_direct_unarchive ? 'checked="true" ' : ''); ?>/>
          </td>
		</tr>

        <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="yandex_direct_status">
                <?php if ($yandex_direct_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
        </tr>
		<?php if ($yandex_direct_token) { ?>
		<tr>
			<td colspan="2"><a onclick="javascript:void(0);" id="check_ads"><span><?php echo $entry_check_ads; ?></span></a></td>
		</tr>
		<?php } ?>
		
<?php } else { ?>
	<tr>
	    <td colspan="2">Зарегистрируйте oAuth приложение, укажите ID и пароль в этой форме, нажмите "Сохранить", далее вновь откройте настройки модуля и введите остальные данные.</td>
	</tr>
<?php } ?>
        </table>
    </form>
  </div>
</div>
<script>
$('#get_token').click(function() {
	var url = 'https://oauth.yandex.ru/authorize?response_type=code&client_id=<?php echo $yandex_direct_id; ?>&display=popup';
	window.open(url, 'ya_oauth','width=600,height=350,resizable=yes,scrollbars=yes,status=yes,location=false');
	return false;
});
$('#check_ads').click(function() {
	var url = '<?php echo $check_ads_url; ?>';
	window.open(url, 'check_ads','width=600,height=350,resizable=yes,scrollbars=yes,status=yes,location=false');
	return false;
});
</script>
<?php echo $footer; ?>