<?php
class ControllerFeedYandexDirect extends Controller {
	public $CONFIG = array(
		'status'=>true,
		'id' => '',
		'password' => '',
		'token'=>'',
		'campaign_id'=>'',
		'stop_status'=>array(),
		'start_status'=>array(),
		'unarchive'=>'',
		'number'=>2000
	);
	
	public function index() {
		$this->load->language('feed/yandex_direct');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('yandex_direct', $this->request->post);	

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
			return;
		}
		
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('yandex_direct_'.$key);
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_id'] = $this->language->get('entry_id');
		$this->data['entry_register_id'] = $this->language->get('entry_register_id');
		$this->data['entry_find_id'] = $this->language->get('entry_find_id');
		$this->data['entry_id_help'] = sprintf($this->language->get('entry_id_help'), HTTP_CATALOG.'index.php?route=feed/yandex_direct');
		$this->data['entry_password'] = $this->language->get('entry_password');
		$this->data['entry_token'] = $this->language->get('entry_token');
		$this->data['entry_campaign_id'] = $this->language->get('entry_campaign_id');
		$this->data['entry_get_token'] = $this->language->get('entry_get_token');
		$this->data['entry_start_status'] = $this->language->get('entry_start_status');
		$this->data['entry_stop_status'] = $this->language->get('entry_stop_status');
		$this->data['entry_stop_status'] = $this->language->get('entry_stop_status');
		$this->data['entry_unarchive'] = $this->language->get('entry_unarchive');
		$this->data['entry_number'] = $this->language->get('entry_number');
		$this->data['entry_check_ads'] = $this->language->get('entry_check_ads');
		$this->data['check_ads_url'] = HTTP_CATALOG.'index.php?route=feed/yandex_direct/check_ads';
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('feed/yandex_direct', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('feed/yandex_direct', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'); 		
		
		
		$this->load->model('localisation/stock_status');
		$stock_statuses = array();
		foreach ($this->model_localisation_stock_status->getStockStatuses() as $item) {
			$stock_statuses[$item['stock_status_id']] = $item['name'];
		}
		$this->data['stock_statuses'] = $stock_statuses;
		
		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['yandex_direct_'.$key])) {
				$this->data['yandex_direct_'.$key] = $this->request->post['yandex_direct_'.$key];
			} else {
				$this->data['yandex_direct_'.$key] = $conf;
			}
		}
		
		$this->template = 'feed/yandex_direct.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'feed/yandex_direct')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}