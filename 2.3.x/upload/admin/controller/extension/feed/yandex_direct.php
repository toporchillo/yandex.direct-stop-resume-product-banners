<?php
class ControllerExtensionFeedYandexDirect extends Controller {
	public $CONFIG = array(
		'status'=>true,
		'id' => '',
		'password' => '',
		'token'=>'',
		'campaign_id'=>'',
		'stop_status'=>array(),
		'start_status'=>array(),
		'unarchive'=>'',
		'number'=>2000
	);
	
	public function index() {
		$this->load->language('extension/feed/yandex_direct');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('yandex_direct', $this->request->post);	

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=feed', 'SSL'));
			return;
		}
		
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('yandex_direct_'.$key);
		}
		
		$data = array();
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_edit'] = $this->language->get('text_edit');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_id'] = $this->language->get('entry_id');
		$data['entry_register_id'] = $this->language->get('entry_register_id');
		$data['entry_find_id'] = $this->language->get('entry_find_id');
		$data['entry_id_help'] = sprintf($this->language->get('entry_id_help'), HTTP_CATALOG.'index.php?route=feed/yandex_direct');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_token'] = $this->language->get('entry_token');
		$data['entry_campaign_id'] = $this->language->get('entry_campaign_id');
		$data['entry_get_token'] = $this->language->get('entry_get_token');
		$data['entry_start_status'] = $this->language->get('entry_start_status');
		$data['entry_stop_status'] = $this->language->get('entry_stop_status');
		$data['entry_stop_status'] = $this->language->get('entry_stop_status');
		$data['entry_unarchive'] = $this->language->get('entry_unarchive');
		$data['entry_number'] = $this->language->get('entry_number');
		$data['entry_check_ads'] = $this->language->get('entry_check_ads');
		$data['check_ads_url'] = HTTP_CATALOG.'index.php?route=extension/feed/yandex_direct/check_ads';
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_feed'),
			'href'      => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=feed', 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/feed/yandex_direct', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['action'] = $this->url->link('extension/feed/yandex_direct', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=feed', 'SSL'); 		
		
		
		$this->load->model('localisation/stock_status');
		$stock_statuses = array();
		foreach ($this->model_localisation_stock_status->getStockStatuses() as $item) {
			$stock_statuses[$item['stock_status_id']] = $item['name'];
		}
		$data['stock_statuses'] = $stock_statuses;
		
		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['yandex_direct_'.$key])) {
				$data['yandex_direct_'.$key] = $this->request->post['yandex_direct_'.$key];
			} else {
				$data['yandex_direct_'.$key] = $conf;
			}
		}
		
		$template = 'extension/feed/yandex_direct.tpl';
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view($template, $data));		
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/feed/yandex_direct')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}