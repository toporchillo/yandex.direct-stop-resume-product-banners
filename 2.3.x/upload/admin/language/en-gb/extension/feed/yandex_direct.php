<?php
$_['heading_title']      = 'Яндекс.Директ: старт/стоп объявлений';

// Text
$_['text_feed']          = 'Каналы продвижения';
$_['text_success']       = 'Настройки модуля обновлены!';

// Entry
$_['entry_status']       = 'Статус:';
$_['entry_id']           = 'ID приложения:<br/><span class="help">client_id</span>';
$_['entry_register_id']  = 'зарегистрировать';
$_['entry_find_id']      = 'узнать';
$_['entry_id_help']      = 'Создать новое приложение с правами: <i>"Использование API Яндекс.Директа"</i>;<br/>Callback URL: <i>%s</i>';
$_['entry_password']     = 'пароль приложения:';
$_['entry_token']        = 'Авторизационный токен:';
$_['entry_get_token']    = 'получить';
$_['entry_campaign_id']  = 'ID кампании:<br/><span class="help">Если кампаний несколько - через запятую</span>';
$_['entry_start_status'] = 'Статусы старта показов:';
$_['entry_stop_status']  = 'Статусы остановки показов:';
$_['entry_unarchive']    = 'Доставать объявления из архива:';
$_['entry_number']    = 'Примерное кол-во рекламных объявлений:';
$_['entry_check_ads']    = 'Старт/стоп объявлений';