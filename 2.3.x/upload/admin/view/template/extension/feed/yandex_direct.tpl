<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-config" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo  $text_edit; ?></h3>
      </div>
      <div class="panel-body">
	
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-config" class="form-horizontal">  
		<div class="form-group required">
			<label class="col-sm-2 control-label" for="yandex_direct_id"><?php echo $entry_id; ?></label>
			<div class="col-sm-10">
              <input type="text" name="yandex_direct_id" id="yandex_direct_id" value="<?php echo $yandex_direct_id; ?>" class="form-control" required />
				<a href="https://oauth.yandex.ru/client/new" target="_blank"> <?php echo $entry_register_id; ?></a> /
				<a href="https://oauth.yandex.ru/client/my" target="_blank"> <?php echo $entry_find_id; ?></a>
			  <br/><span class="help"><?php echo $entry_id_help; ?></span>
            </div>
		</div>
		
		<div class="form-group required">
			<label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
			<div class="col-sm-10">
              <input type="password" id="input-password" name="yandex_direct_password" value="<?php echo $yandex_direct_password; ?>" class="form-control" required />
            </div>
		</div>

<?php if ($yandex_direct_id && $yandex_direct_password) { ?>
		<div class="form-group required">
			<label class="col-sm-2 control-label" for="input-token"><?php echo $entry_token; ?></label>
			<div class="col-sm-10">
              <input type="text" id="input-token" name="yandex_direct_token" value="<?php echo $yandex_direct_token; ?>" class="form-control" /> <a href="javascript:void(0);" id="get_token"><?php echo $entry_get_token; ?></a>
            </div>
		</div>
		
		<div class="form-group required">
			<label class="col-sm-2 control-label" for="input-campaign_id"><?php echo $entry_campaign_id; ?></label>
			<div class="col-sm-10">
              <input type="text" id="input-campaign_id" name="yandex_direct_campaign_id" value="<?php echo $yandex_direct_campaign_id; ?>" class="form-control" />  <a href="https://direct.yandex.ru/registered/main.pl" target="_blank"><?php echo $entry_find_id; ?></a>
            </div>
		</div>
		
		<div class="form-group required">
			<label class="col-sm-2 control-label" for="select-stop_status"><?php echo $entry_stop_status; ?></label>
			<div class="col-sm-10">
				<select id="select-stop_status" name="yandex_direct_stop_status[]" size="6" multiple>
				  <?php foreach ($stock_statuses as $stat=>$stat_name) { ?>
				  <option value="<?php echo $stat_name; ?>"<?php echo (is_array($yandex_direct_stop_status) && in_array($stat_name, $yandex_direct_stop_status) ? ' selected="selected"' : ''); ?>><?php echo $stat_name; ?></option>
				  <?php } ?>
				</select>			
            </div>
		</div>
		
		<div class="form-group required">
			<label class="col-sm-2 control-label" for="select-start_status"><?php echo $entry_start_status; ?></label>
			<div class="col-sm-10">
				<select id="select-start_status" name="yandex_direct_start_status[]" size="6" multiple>
				  <?php foreach ($stock_statuses as $stat=>$stat_name) { ?>
				  <option value="<?php echo $stat_name; ?>"<?php echo (is_array($yandex_direct_start_status) && in_array($stat_name, $yandex_direct_start_status) ? ' selected="selected"' : ''); ?>><?php echo $stat_name; ?></option>
				  <?php } ?>
				</select>			
            </div>
		</div>

		<div class="form-group required">
			<label class="col-sm-2 control-label" for="input-direct_number"><?php echo $entry_number; ?></label>
			<div class="col-sm-10">
				<input type="text" name="yandex_direct_number" value="<?php echo intval($yandex_direct_number); ?>" />		
            </div>
		</div>

		<div class="form-group required">
			<label class="col-sm-2 control-label" for="checkbox-unarchive"><?php echo $entry_unarchive; ?></label>
			<div class="col-sm-10">
				<input type="checkbox" name="yandex_direct_unarchive" value="1" <?php echo ($yandex_direct_unarchive ? 'checked="true" ' : ''); ?>/>
            </div>
		</div>
		
		<div class="form-group required">
			<label class="col-sm-2 control-label" for="select-status"><?php echo $entry_status; ?></label>
			<div class="col-sm-10">
			  <select name="yandex_direct_status">
                <?php if ($yandex_direct_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
			</div>
		</div>
		
		<?php if ($yandex_direct_token) { ?>
		<div class="form-group required">
			<div class="col-sm-12">
			<a href="javascript:void(0);" id="check_ads"><span><?php echo $entry_check_ads; ?></span></a>
			</div>
		</div>
		<?php } ?>
		
<?php } else { ?>
		<div class="form-group required">
			<div class="col-sm-12">
			Зарегистрируйте oAuth приложение, укажите ID и пароль в этой форме, нажмите "Сохранить", далее вновь откройте настройки модуля и введите остальные данные.
			</div>
		</div>
<?php } ?>
      </form>
    </div>
  </div>
  </div>
</div>
<script>
$('#get_token').click(function() {
	var url = 'https://oauth.yandex.ru/authorize?response_type=code&client_id=<?php echo $yandex_direct_id; ?>&display=popup';
	window.open(url, 'ya_oauth','width=600,height=350,resizable=yes,scrollbars=yes,status=yes,location=false');
	return false;
});
$('#check_ads').click(function() {
	var url = '<?php echo $check_ads_url; ?>';
	window.open(url, 'check_ads','width=600,height=350,resizable=yes,scrollbars=yes,status=yes,location=false');
	return false;
});
</script>
<?php echo $footer; ?>