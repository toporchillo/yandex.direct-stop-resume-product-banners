<?php
class ControllerExtensionFeedYandexDirect extends Controller {
	const LOG_ECHO = true;	//Выводиль ли сообщения для лога
	const LOG_LEVEL = 2;	//Уровень детальности лога
	//private $seo_controller;
	private $to_stop = array();
	private $to_start = array();
	private $to_unarchive = array();

	private function fault($message) {
		echo '<h3>'.$message.'</h3>';
		exit;
	}
	
	public function index() {
		if (!isset($_GET['code'])) {
			return $this->fault('Яндекс вернул неверный или пустой код авторизации OAuth');
		}
		$client_id = $this->config->get('yandex_direct_id');
		$password = $this->config->get('yandex_direct_password');
		if (!$client_id || !$password) {
			return $this->fault('Сохраните в настройках модуля ID и пароль <a href="https://oauth.yandex.ru/client/my" target="_blank">приложения</a>.');
		}
		
		$tuCurl = curl_init();
		curl_setopt($tuCurl, CURLOPT_URL, 'https://oauth.yandex.ru/token');
		curl_setopt($tuCurl, CURLOPT_PORT , 443);
		curl_setopt($tuCurl, CURLOPT_HEADER, 0);
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($tuCurl, CURLOPT_POST, 1);
		curl_setopt($tuCurl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($tuCurl, CURLOPT_SSL_VERIFYHOST, false);		
		curl_setopt($tuCurl, CURLOPT_POSTFIELDS, array(
			'grant_type'=>'authorization_code',
			'code'=>$_GET['code'],
			'client_id'=>$client_id,
			'client_secret'=>$password
		));
		$tuData = curl_exec($tuCurl);
		if(curl_errno($tuCurl)){
			$info = curl_getinfo($tuCurl);
			return $this->fault('Ошибка доступа к серверу авторизации: '.curl_error($tuCurl).'. Потрачено: ' . $info['total_time'] . 'сек. URL: ' . $info['url']);
		} 
		
		//Yandex returns {"access_token": "ea135929105c4f29a0f5117d2960926f", "expires_in": 2592000}
		$authdata = json_decode($tuData);
		
		header('Content-Type: text/html; charset=utf-8');
		if (isset($authdata->access_token) && $authdata->access_token) {
			echo '<h3>Токен: <u>'.$authdata->access_token.'</u><br/>Время жизни: '.floor($authdata->expires_in/3600/24).' суток.</h3>';
			echo 'Сохраните токен в настройках модуля, обновляйте токен до того, как кончится его время жизни.';
			exit;
		}
		else {
			return $this->fault('oAuth авторизация не удалась. Закройте это окно и попробуйте получить токен еще раз.');
		}
	}
	
	public function check_ads() {
		//if (isset($_SERVER['HTTP_HOST']) && $this->LOG_ECHO) {
			header("Content-Type: text/html; charset=utf-8");
		//}
		if (!$this->config->get('yandex_direct_stop_status')) {
			$this->log('Модуль "Яндекс.Директ: старт/стоп объявлений" отключен', 1);
			return;
		}
		include DIR_SYSTEM.'/library/directApi.php';
		$api = new DirectApi();
		$api->init($this->config->get('yandex_direct_id'), $this->config->get('yandex_direct_password'), false);
		$api->setToken($this->config->get('yandex_direct_token'));
		$chunks = ceil(intval($this->config->get('yandex_direct_number'))/9000);

		for ($i=0; $i<$chunks; $i++) {
			
			try {
				$req_data = array(
					'SelectionCriteria'=> array(
						'CampaignIds' => array_map('intval', explode(',', $this->config->get('yandex_direct_campaign_id'))),
					),
					'FieldNames' => array('Id', 'CampaignId', 'Status', 'State'),
                    'TextAdFieldNames' => array('Href'),
					'Page' => array(
						'Limit' => 9000,
						'Offset' => $i*9000
					)
					//'Filter' => array('StatusArchive' => array('No'))
				);
				/*
				if ($this->config->get('yandex_direct_unarchive')) {
					unset($req_data['Filter']);
				}
				*/
				$res = $api->ads_get($req_data);
				$error = $api->getErrorStr();
				if ($error) {
					$this->log('Ошибка обращения к Директ.API, метод get ('.json_encode($req_data).') - '.$error, 2);
				}
				elseif (isset($res['result']) && isset($res['result']['Ads']) && count($res['result']['Ads']) > 0) {
					$this->load->model('catalog/product');
					$this->to_start = array();
					$this->to_unarchive = array();
					$this->to_stop = array();
				
					foreach($res['result']['Ads'] as $banner) {
						$this->handleBanner($banner);
					}
					
					if (count($this->to_stop) > 0) {
						$res = $api->ads_suspend(array('SelectionCriteria' => array('Ids' => $this->to_stop)));
						$error = $api->getErrorStr();
						if ($error) {
							$this->log('Ошибка обращения к Директ.API, метод suspend - '.$error, 2);
						}
						elseif (isset($res['result'])) {
                            //print_r($res['result']['SuspendResults']);
							$this->log(count($this->to_stop).' объявлений остановлено ('.implode(',', $this->to_stop).')', 2);
						}
					}
                    
					if ($this->config->get('yandex_direct_unarchive') && count($this->to_unarchive) > 0) {
						$res = $api->ads_unarchive(array('SelectionCriteria' => array('Ids' => $this->to_unarchive)));
						$error = $api->getErrorStr();
						if ($error) {
							$this->log('Ошибка обращения к Директ.API, метод unarchive - '.$error, 2);
						}
						elseif (isset($res['result'])) {
                            //print_r($res['result']['UnarchiveResults']);
							$this->log(count($this->to_start).' объявлений из архива ('.implode(',', $this->to_unarchive).')', 2);
						}
					}
                    
					if (count($this->to_start) > 0) {
						$res = $api->ads_resume(array('SelectionCriteria' => array('Ids' => $this->to_start)));
						$error = $api->getErrorStr();
						if ($error) {
							$this->log('Ошибка обращения к Директ.API, метод resume: '.$error, 2);
						}
						elseif (isset($res['result'])) {
							$this->log(count($this->to_start).' объявлений запущено ('.implode(',', $this->to_start).')', 2);
						}
					}
				}
				else {
					$this->log('Яндекс не вернул данные об объявлениях. Возможно у вас нет объявлений, либо объявления закончились.', 2);
					break;
				}
			}
			catch (Exception $e) {
				$this->log('Ошибка: '.$e->getMessage());
			}
		}
	}

	/**
	* Обработка рекламного объявления
	* Формируются массивы $this->to_stop и $this->to_start для дальнейших действий
	*/
	protected function handleBanner($banner) {
        if ($banner['Status'] != 'ACCEPTED') {
			$this->log('Banner ID: '.$banner['Id'].', Is not Accepted - SKIP.', 3);
            return;
        }
        if (!isset($banner['TextAd']) || !isset($banner['TextAd']['Href']) || !$banner['TextAd']['Href']) {
			$this->log('Banner ID: '.$banner['Id'].', Not a text banner - SKIP.', 3);
            /**
             * @todo сделать обработку и других типов объявлений
             */
            return;
        }
        $banner['Href'] = $banner['TextAd']['Href'];
        unset($banner['TextAd']);
        
		$urldata = parse_url(HTTP_SERVER);
        $bnr_urldata = parse_url($banner['Href']);
		if (strtolower($bnr_urldata['host']) != strtolower($urldata['host'])) {
			$this->log('Banner ID: '.$banner['Id'].', Domain: '.$bnr_urldata['host'].' - SKIP.', 3);
			return;
		}
        /**
         * @todo сделать кэширование товаров и категорий в зависимости от URL
         */
		$product_id = $this->getProductOnUrl($banner['Href']);
		if ($product_id == -1) {
			$this->log('Banner ID: '.$banner['Id'].', Page not found: '.($this->LOG_LEVEL > 2 ? ', URL: '.$banner['Href'] : '').' - STOP.', 2);
			$this->to_stop[] = $banner['Id'];
			return;
		}
		elseif (!$product_id) {
            /*
            $category_id = $this->getCategoryOnUrl($banner['Href']);
            if ($category_id == -1) {
                $this->log('Banner ID: '.$banner['Id'].', Page not found, URL: '.$banner['Href'].' - STOP.', 2);
                $this->to_stop[] = $banner['Id'];
                return;
            }
            elseif (!$category_id) {
            */
                $this->log('Banner ID: '.$banner['Id'].', Neither product nor a category page '.($this->LOG_LEVEL > 2 ? ', URL: '.$banner['Href'] : '').' - SKIP.', 3);
                return;
            /*
            }
            $state = $this->getCategoryState($category_id);
            */
		}
        else {
            $state = $this->getProductState($product_id);
        }
        
		if ($state == 0 && $banner['State'] == 'ON') {
			$this->log('Banner ID: '.$banner['Id'].($this->LOG_LEVEL > 4 ? ', URL: '.$banner['Href'] : '').' - to STOP.', 2);
			$this->to_stop[] = $banner['Id'];
			return;
		}
		if ($state == 1 && $banner['State'] == 'ARCHIVED') {
			$this->to_unarchive[] = $banner['Id'];
			$this->log('Banner ID: '.$banner['Id'].($this->LOG_LEVEL > 4 ? ', URL: '.$banner['Href'] : '').' - to UNARCHIVE.', 2);
			$this->to_start[] = $banner['Id'];
			$this->log('Banner ID: '.$banner['Id'].($this->LOG_LEVEL > 4 ? ', URL: '.$banner['Href'] : '').' - to START.', 2);
			return;
		}
        if ($state == 1 && $banner['State'] == 'SUSPENDED') {
			$this->to_start[] = $banner['Id'];
			$this->log('Banner ID: '.$banner['Id'].($this->LOG_LEVEL > 4 ? ', URL: '.$banner['Href'] : '').' - to START.', 2);
			return;
        }        
		if ($state == 0 && $banner['State'] != 'ON') {
			$this->log('Banner ID: '.$banner['Id'].($this->LOG_LEVEL > 4 ? ', URL: '.$banner['Href'] : '').' - already STOPPED.', 3);
			return;
		}
		if ($state == 1 && $banner['State'] == 'ON') {
			$this->log('Banner ID: '.$banner['Id'].($this->LOG_LEVEL > 4 ? ', URL: '.$banner['Href'] : '').' - already STARTED.', 3);
			return;
		}
		if ($state == -1) {
			$this->log('Banner ID: '.$banner['Id'].($this->LOG_LEVEL > 4 ? ', URL: '.$banner['Href'] : '').' - SKIP.', 3);
			return;
		}
	}

	/**
	* Получение ID категории по ее URL. Работает для SEO_PRO и SEO_URL, для других ЧПУ может не работать
	*/
	protected function getCategoryOnUrl($href) {
		$components = parse_url($href);
		$seo_url = true;
		if (isset($components['query'])) {
			$query = array();
			parse_str($components['query'], $query);
			if (isset($query['route']) && $query['route'] == 'product/category' && isset($query['category_id'])) {
				return $query['category_id'];
			}
		}
        /**
         * @todo Сделать получение категории по ее ЧПУ
         */
		return false;
    }
    
	/**
	* Возвращает статус объявления по товару:
	* 0 - остановить показы
	* 1 - запустить показы
	* -1 - оставить как есть
	*/
	protected function getCategoryState($category_id) {
        /**
         * @todo написать метод определения статуса категории по товарам в ней
         */
	}
	
	/**
	* Получение ID товара по его URL. Работает для SEO_PRO и SEO_URL, для других ЧПУ может не работать
	*/
	protected function getProductOnUrl($href) {
		$components = parse_url($href);
		$seo_url = true;

		if (isset($components['query'])) {
			$query = array();
			parse_str($components['query'], $query);
			if (isset($query['route']) && $query['route'] == 'product/product' && isset($query['product_id'])) {
				return $query['product_id'];
			}
		}
		if (isset($components['path'])) {
			$parts = explode('/', $components['path']);
			array_shift($parts);
			$parts = array_filter($parts, 'strlen');
			if (count($parts)>0) {
				$part = $parts[count($parts)-1];
				if ($this->config->get('config_seo_url_postfix')) {
					$part = str_replace($this->config->get('config_seo_url_postfix'), '', $part);
				}
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "' AND query LIKE 'product_id%'");
				
				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);
					
					if ($url[0] == 'product_id') {
						return $url[1];
					}
				}
			}
		}
		return false;
	}
	
	/**
	* Возвращает статус объявления по товару:
	* 0 - остановить показы
	* 1 - запустить показы
	* -1 - оставить как есть
	*/
	protected function getProductState($product_id) {
		$product = $this->model_catalog_product->getProduct($product_id);
		$stop_status = $this->config->get('yandex_direct_stop_status');
		$start_status = $this->config->get('yandex_direct_start_status');
		if (!$product['status']) {
			return 0;
		}
		if (!isset($product['stock_status'])) {
			return 0;
		}
		//Если на складе 0, то смотрим статус товара, когда его нет на складе
		if (($product['quantity']+$product['isbn']) <= 0) {
			if (is_array($stop_status) && in_array($product['stock_status'], $stop_status))
				return 0;
			elseif (is_array($start_status) && in_array($product['stock_status'], $start_status))
				return 1;
		}
		//Если товар есть на складе, то запускаем показы
		else {
			return 1;
		}
		return -1;
	}
	
	/**
	* Писать в журнал ошибки и сообщения
	* @param str $msg запись
	* @param int $level приоритет ошибки/сообщения. Если приоритет больше self::LOG_LEVEL, то он записан не будет
	**/
	private function log($msg, $level = 0) {
		if ($level > self::LOG_LEVEL) return;
		$fp = fopen(DIR_LOGS.'yadirect_checker.log', 'a');
		fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
		if (self::LOG_ECHO) echo nl2br(htmlspecialchars($msg))."<br/>\n";
		fclose($fp);
	}	
}
